package ppa.spring.core.iocid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ppa.spring.core.iocid.beans.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println( context.getApplicationName() );
        
        // access to the Customer Bean
        Customer customer = context.getBean("customer", Customer.class);
        
    }
}
