# SPRING - IOC - ID

## Presentation
This is a simple spring project to describe core concepts

## Installation
simple maven project.

__Dependencies :__  
*sring framework bom*

```
<!-- https://mvnrepository.com/artifact/org.springframework/spring-framework-bom -->  
<dependencyManagement>
	<dependencies>
		<dependency>  
		    <groupId>org.springframework</groupId>  
		    <artifactId>spring-framework-bom</artifactId>  
		    <version>5.1.9.RELEASE</version>  
		    <type>pom</type>  
		</dependency>
	<dependencies>
</dependencyManagement>
```

Then, we can import spring module dependecy without version tag. 

## Introduction to the Spring IoC Container and Beans
What is IoC implementation (also known as Dependency Injection - DI) ?  
It is a process whereby objects define their dependencies only through :
<ul>
	<li>Constructor arguments.</li>
	<li>Arguments to a factory method.</li>
	<li>Properties that are set on the object instance after it is constructed.</li>
</ul>
The container then injects those dependencies when it creates the bean.
<ul>
	<li>`org.springframework.beans` and `org.springframework.context` are the basis packages for Spring Framework's IoC container.</li>
	<li>`BeanFactory` interface provides an advanced configuration mechanism capable of managing any type of obect.</li>
	<li>`ApplicationContext` is a sub-interface of `BeanFactory`.</li>
	<li>Beans form the backbone of spring application.</li>
</ul>

## Container Overview
##### The container read configuration metadata either from :
<ul>
	<li>xml configuration file.</li>
	<li>Java annotations.</li>
	<li>Java code.</li>
</ul>

##### Some implementations of ApplicationContext :
<ul>
	<li>`ClassPathXmlApplicationContext`</li>
	<li>`FileSystemXmlApplicationContext`</li>
</ul>

##### Configuration Metadata

##### Instantiating a Container

*Composing XML-based Configuration Metadata*

```
<import resource="configurationFile.xml />
```
All imported files must be in the same directory or classpath location of the file doing the importing.
It is possible, but NOT RECOMMANDED, to reference files in the parent directory using relative `"../"` path.  
We can also use fully qualified resource locations.

*The Groovy Bean Definition DSL*

##### Using the Container

## Bean Overview
Within container itself, these bean definitions are represented as `BeanDefinition` object.

##### Naming beans
<ul>
	<li>id attribute lets us specify exactly one id</li>
	<li>name attribute, for more aliases of the bean (separated by comma, semicolon or white space)</li>
	<li>if neither id or name are specified, the container generates a unique name for the bean</li>
</ul>

*Aliasing a Bean outside the Bean Definition*

```
<alias name="fromName" alias="toName" />
```
`@Bean` Java-configuration annotation can be used to provide aliases.

##### Instantiating Beans
*Intantiation with a Constructor*

```
<bean id="exampleBean" class="examples.ExampleBean"/>
```

*Instantiation with a Static Factory Method*

```
<bean id="clientService"
    class="examples.ClientService"
    factory-method="createInstance"/>
```
... and the static factory method :

```
public class ClientService {
    private static ClientService clientService = new ClientService();
    private ClientService() {}

    public static ClientService createInstance() {
        return clientService;
    }
}
```

*Instantiation by Using an Instance Factory Method*

```
<!-- the factory bean, which contains a method called createInstance() -->
<bean id="serviceLocator" class="examples.DefaultServiceLocator">
    <!-- inject any dependencies required by this locator bean -->
</bean>

<!-- the bean to be created via the factory bean -->
<bean id="clientService"
    factory-bean="serviceLocator"
    factory-method="createClientServiceInstance"/>
```

```
public class DefaultServiceLocator {

    private static ClientService clientService = new ClientServiceImpl();

    public ClientService createClientServiceInstance() {
        return clientService;
    }
}
```

*Determining a Bean’s Runtime Type*

## Dependencies
##### Dependency Injection
Dependency injection (DI) is a process whereby objects define their dependencies (that is, the other objects with which they work) only through constructor arguments, arguments to a factory method, or properties that are set on the object instance after it is constructed or returned from a factory method.

*Constructor-based Dependency Injection*  
*Constructor Argument Resolution*  

```
<beans>
    <bean id="beanOne" class="x.y.ThingOne">
        <constructor-arg ref="beanTwo"/>
        <constructor-arg ref="beanThree"/>
    </bean>

    <bean id="beanTwo" class="x.y.ThingTwo"/>

    <bean id="beanThree" class="x.y.ThingThree"/>
</beans>
```

<ul>
	<li>Constructor argument type matching</li>
	<li>Constructor argument index</li>
	<li>Constructor argument name</li>
</ul>

*Setter-based Dependency Injection*  
*Dependency Resolution Process*  